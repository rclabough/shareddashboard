﻿using ImageOTheDay.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace ImageOTheDay.Services
{
    public class ConfigService
    {
        public ConfigModel GetConfig()
        {
            var model = new ConfigModel()
            {
                Url = ConfigurationManager.AppSettings["ProjectUrl"]
            };
            return model;
        }
    }
}