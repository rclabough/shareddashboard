﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ImageOTheDay.Services
{
    public class ConnectedServices
    {
        public static ConnectedServices Instance => new ConnectedServices();
        private ConnectedServices()
        {

        }

        private ConcurrentDictionary<string, List<string>> allowedChannels
            = new ConcurrentDictionary<string, List<string>>();

        public void Register(string token, List<string> channels)
        {
            allowedChannels[token] = channels;
        }

        public IEnumerable<string> Retrieve(string token)
        {
            var list = allowedChannels.ContainsKey(token) ? allowedChannels[token] : null;
            return list;
        }

    }
}