﻿using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(ImageOTheDay.Startup))]
namespace ImageOTheDay
{
    
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // For more information on how to configure your application, visit http://go.microsoft.com/fwlink/?LinkID=316888
            app.MapSignalR("/signalr", new HubConfiguration { EnableJSONP = true, EnableDetailedErrors = true });
        }
    }
}