﻿(function () {
    var app = angular.module('pageoftheday');

    app.controller('FrameController',
        function ($scope, $sce, callback, signalrservice, signalrhubs) {

            var updateData = function (link) {
                var filetype = link.split('.').pop();
                console.log('Filetype is ' + filetype);
                if (filetype == "mp4") {
                    $scope.video = true;
                    $scope.image = false;
                    console.log('Found Video');
                } else {
                    $scope.video = false;
                    $scope.image = true;
                    console.log('Found Image');
                }
                $scope.setProject(link);
            }

            signalrservice.Initialize(function() {
                $scope.title = 'Page of the Day';
                $scope.id = callback.configuration.id;
                $scope.url = callback.configuration.callback;
                $scope.user = '';
                $scope.usermessage = '';
                $scope.messagesupplied = false;
                $scope.$apply();

                
                signalrhubs.setOnPublish(function (data) {
                    console.log('Received: ' + data);
                    console.log(data);
                    
                    updateData(data.link);

                    $scope.user = data.user;
                    $scope.usermessage = data.message;
                    $scope.$apply();
                });
            });

            $scope.setProject = function (loc) {
                $scope.currentProjectUrl = $sce.trustAsResourceUrl(loc);
            }

            
        });
})();