﻿(function() {
    var app = angular.module('pageoftheday');

    app.factory('callback',
        function () {
            return {
                configuration: window.configuration
            }
        });
})();