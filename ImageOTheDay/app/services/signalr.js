﻿(function() {
    var app = angular.module('pageoftheday');

    app.factory("signalrservice", function ($http, callback, signalrhubs) {
        var baseUrl = callback.configuration.callback;
        var id = callback.configuration.id;

        var init = function (done) {

            //jQuery.support.cors = true;
            $.connection.hub.url = baseUrl + "/signalr";
            console.log('Initializing SignalR at URL ' + $.connection.hub.url);
            var slackHub = $.connection.slack;
            slackHub.client.publish = function (link, user, message) {
                signalrhubs.OnPublish(link, user, message);
            }

            // Turn logging on so we can see the calls in the browser console
            $.connection.logging = true;
            console.log('Initializing signalr with url ' + $.connection.hub.url);
            $.connection.hub.start({ jsonp: true }).done(function () {
                console.log('Subscribing with gameId ' + id);
                slackHub.server.subscribe(id);
                slackHub.client.publish = function (link, user, message) {
                    console.log('made it here for some reason');
                    signalrhubs.OnPublish(link, user, message);
                }
                if (done) {
                    done();
                }
            });
        }

        return {
            Initialize: function (done) {
                var scriptUrl = baseUrl + '/signalr/hubs';
                //$.ajax({
                //    url: scriptUrl,
                //    dataType: 'script',
                //    success: function () { init(done); },
                //    error: function (error) { init(error.responseText); },
                //    async: true
                //});
                init(done);

            }
        }
    });

    app.factory("signalrhubs", function () {
        var publishFn;

        return {
            OnPublish(link, user, message) {
                publishFn({ link: link, user: user, message: message });
            },
            setOnPublish(callback) {
                publishFn = callback;
            }
        }
    });
})();