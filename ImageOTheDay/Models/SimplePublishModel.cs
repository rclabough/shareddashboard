﻿using System;

namespace ImageOTheDay.Models
{
    public class SimplePublishModel
    {
        public string GroupId { get; set; }
        public string Link { get; set; }
        public string User { get; set; }
        public string Message { get; set; }
    }
}