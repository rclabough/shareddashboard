﻿using System;

namespace ImageOTheDay.Models
{
    public class CallbackModel
    {
        public string CallbackUrl { get; set; }
        public string Id { get; set; }
    }
}