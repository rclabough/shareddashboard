﻿using Microsoft.AspNet.SignalR;

namespace ImageOTheDay.Hubs
{
    public class Slack : Hub
    {
        public Slack()
        {
            HubContainer.SlackHub = this;
        }

        public void Subscribe(string id)
        {
            Groups.Add(Context.ConnectionId, id);
        }
        

        public void Publish(string id, string url)
        {
            var group = Clients.Group(id);
            group?.publish(url);
        }
        
    }
}