﻿using System;
using System.Web.Mvc;
using ImageOTheDay.Hubs;
using ImageOTheDay.Models;
using Microsoft.AspNet.SignalR;
using System.Collections.Generic;
using System.Collections.Concurrent;
using ImageOTheDay.Services;
using System.Linq;

namespace ImageOTheDay.Controllers
{
    public class SlackController : Controller
    {
        [HttpGet]
        public ActionResult Index(string slackToken, string channel)
        {
            var model = new CallbackModel
            {
                CallbackUrl = new ConfigService().GetConfig()?.Url,
                Id = TokenJoin(slackToken ?? Guid.NewGuid().ToString(), channel)
            };
            return View(model);
        }

        [HttpGet]
        public JsonResult Publish(SimplePublishModel data)
        {
            var slackHub = GlobalHost.ConnectionManager.GetHubContext<Slack>();
            if (slackHub == null)
            {
                throw new NullReferenceException("Slack not defined yet?");
            }
            slackHub.Clients.Group(data.GroupId.ToString()).Publish(data.Link, data.User, data.Message);
            return Json(new {result = "ok"}, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Publish(SlackCommandModel model)
        {
            try
            {
                var textParts = model.text.Split(' ');

                if (textParts[0] == "whoami")
                {
                    return Json(new { text = BuildUri(model.token, model.channel_id) });
                }
                
                var slackHub = GlobalHost.ConnectionManager.GetHubContext<Slack>();
                if (slackHub == null)
                {
                    throw new NullReferenceException("Slack not defined yet?");
                }

                var id = TokenJoin(model.token, model.channel_id);
                
                var message = textParts.Length > 1 ? string.Join(" ", textParts.Take(textParts.Length - 1)) : string.Empty;
                var link = textParts.Length > 1 ? textParts[textParts.Length - 1] : textParts[0];

                slackHub.Clients.Group(id).Publish(link, model.user_name, message);
                return Json(new { text = $"{model.user_name} updated the board!" });
            } catch (Exception ex)
            {
                return Json(new { message = ex.Message, stacktrace = ex.StackTrace, token = model.token, channel = model.channel_name, channelid = model.channel_id });
            }
        }

        private string TokenJoin(string token, string channel)
        {
            return $"{token},{channel}";
        }

        private string BuildUri(string token, string channel)
        {
            var baseUrl = new ConfigService().GetConfig().Url;
            return $"{baseUrl}/Slack?slackToken={token}&channel={channel}";
        }
    }
}